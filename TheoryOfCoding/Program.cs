﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheoryOfCoding
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] EndMas = new int[4, 5];
            int[,] Mas = new int[4,5];
            Mas[0, 0] = 1;
            Mas[0, 1] = 1;
            Mas[0, 2] = 1;
            Mas[0, 3] = 1;
            Mas[1, 0] = 0;
            Mas[1, 1] = 0;
            Mas[1, 2] = 0;
            Mas[1, 3] = 0;

            Console.WriteLine("Our mass");
            Print(Mas);
            Console.WriteLine();
            Code(Mas);
            Console.WriteLine("Coding mass");
            Print(Mas);
            
            int x, y;
            Console.WriteLine("Where are place you want doing the error?");
            Console.WriteLine("Please enter coordinates");
            x = Int32.Parse(Console.ReadLine()) - 1;
            y = Int32.Parse(Console.ReadLine()) - 1;
            if (Mas[x, y] == 0)
            {
                Mas[x, y] = 1;
            }
            else {
                Mas[x, y] = 0;
            }
            Print(Mas);

            Console.WriteLine("We send mass:{0}", Send(Mas));
            TakingString(EndMas, Send(Mas));
            Console.WriteLine("Take mass...");
            
            Print(EndMas);
            Console.WriteLine("");
            DeCoding(EndMas);
            Console.WriteLine("");


            Console.WriteLine("Decoding of Class Leader:");
            LeaderClass(EndMas);

            Console.ReadKey();
        }

        static void LeaderClass(int[,] arrs)
        {

            int[,] Code1 = new int[4, 5];
            Code1[0, 0] = 0;
            Code1[0, 1] = 1;
            Code1[0, 2] = 1;
            Code1[0, 3] = 1;
            Code1[1, 0] = 1;
            Code1[1, 1] = 0;
            Code1[1, 2] = 1;
            Code1[1, 3] = 1;
            Code1[2, 0] = 1;
            Code1[2, 1] = 1;
            Code1[2, 2] = 0;
            Code1[2, 3] = 1;
            Code1[3, 0] = 1;
            Code1[3, 1] = 1;
            Code1[3, 2] = 1;
            Code1[3, 3] = 0;


            int[,] Code2 = new int[4, 5];
            Code2[0, 0] = 1;
            Code2[0, 1] = 0;
            Code2[0, 2] = 0;
            Code2[0, 3] = 0;
            Code2[1, 0] = 0;
            Code2[1, 1] = 1;
            Code2[1, 2] = 0;
            Code2[1, 3] = 0;
            Code2[2, 0] = 0;
            Code2[2, 1] = 0;
            Code2[2, 2] = 1;
            Code2[2, 3] = 0;
            Code2[3, 0] = 0;
            Code2[3, 1] = 0;
            Code2[3, 2] = 0;
            Code2[3, 3] = 1;

            bool bo;
            int temp;
            string tempInd = "";
            for (int i = 0; i < arrs.GetLength(0)-1; i++)
            {
                bo = true;
                temp = 0;
                for (int j = 0; j < arrs.GetLength(1)-1; j++)
                {
                    if (Code2[i,j]!=arrs[i,j])
                    {
                        temp = temp + 1;
                        tempInd = tempInd + i + ", " + j;
                    }
                }
                if (temp == 1)
                {
                    Console.WriteLine("Error in {0} place", tempInd);
                    bo = false;
                }
                if (temp == 0)
                {
                    bo = false;
                }
                if (bo)
                {
                    Console.WriteLine("Errors over then 1!");
                }

            }
        }
        
        static void Print(int[,] arrs)
        {
            for (int i = 0; i < arrs.GetLength(0); i++)
            {
                for (int j = 0; j < arrs.GetLength(1); j++)
                {
                    Console.Write(arrs[i,j] + "; ");
                }
                Console.WriteLine();
            }
        }
        
        static void Code(int[,] arrs)
        {
            int sum;
            for (int i = 0; i < arrs.GetLength(0); i++)
            {
                sum = 0;
                for (int j = 0; j < arrs.GetLength(1)-1; j++)
                {
                    sum = sum + arrs[i, j];
                }
                if (sum % 2 == 0)
                {
                    arrs[i, arrs.GetLength(1)-1] = 0;
                }
                else
                {
                    arrs[i, arrs.GetLength(1)-1] = 1;
                }
            }
            
            for (int i = 0; i < arrs.GetLength(1); i++)
            {
                sum = 0;
                for (int j = 0; j < arrs.GetLength(0) - 1; j++)
                {
                    sum = sum + arrs[j, i];
                }
                if (sum % 2 == 0)
                {
                    arrs[arrs.GetLength(0) - 1, i] = 0;
                }
                else
                {
                    arrs[arrs.GetLength(0) - 1, i] = 1;
                }
            }            
        }
        static string Send(int[,] arrs)
        {
            string sendString = "";
            for (int i = 0; i < arrs.GetLength(0); i++)
            {
                for (int j = 0; j < arrs.GetLength(1); j++)
                {
                    sendString = sendString + arrs[i, j];
                }
            }
            return sendString;
        }
        static void TakingString(int[,] arrs, string takingString )
        {
            
            int iterator=0;
            for (int i = 0; i < arrs.GetLength(0); i++)
            {
                for (int j = 0; j < arrs.GetLength(1); j++)
                {
                    arrs[i, j] = Int32.Parse(takingString[iterator].ToString());
                    iterator = iterator + 1;
                }
            }
        }

        static void DeCoding(int[,] arrs)
        {
            int sum, x = -1, y = -1;
            for (int i = 0; i < arrs.GetLength(0); i++)
            {
                sum = 0;
                for (int j = 0; j < arrs.GetLength(1) - 1; j++)
                {
                    sum = sum + arrs[i, j];
                }
                if (sum % 2 != arrs[i, arrs.GetLength(1) - 1])
                {
                    x = i;
                }
            }

            for (int i = 0; i < arrs.GetLength(1); i++)
            {
                sum = 0;
                for (int j = 0; j < arrs.GetLength(0) - 1; j++)
                {
                    sum = sum + arrs[j, i];
                }
                if (sum % 2 != arrs[arrs.GetLength(0) - 1, i])
                {
                   y = i;
                }
            }

            if (x >= 0 & y >= 0 )
            {
                 Console.WriteLine("And have the error in {0} line, and {1} row", x+1, y+1);
            }
        }
    }
}
